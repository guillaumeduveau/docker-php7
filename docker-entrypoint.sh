#!/bin/sh
set -e

# Run PHP-FPM as www-data with different UID/GID.
if [ -n "${PHP_UID}" ] && [ -n "${PHP_GID}" ]
then
  deluser www-data
  addgroup -g ${PHP_GID} -S www-data
  adduser -u ${PHP_UID} -D -S -s /bin/bash -G www-data www-data
  sed -i '/^www-data/s/!/*/' /etc/shadow
fi

# Set PHP
if [ -n "${PHP_DATE_TIMEZONE}" ]
then
  sed -i 's,date.timezone = Europe/Paris,'"${PHP_DATE_TIMEZONE}"',g' /etc/php7/conf.d/50_php.ini
fi

if [ -n "${PHP_MAX_EXECUTION_TIME}" ]
then
  sed -i 's/max_execution_time = 600/max_execution_time = '"${PHP_MAX_EXECUTION_TIME}"',g' /etc/php7/conf.d/50_php.ini
fi

if [ -n "${PHP_UPLOAD_MAX_FILESIZE}" ]
then
  sed -i 's/upload_max_filesize = 100M/upload_max_filesize = '"${PHP_UPLOAD_MAX_FILESIZE}"',g' /etc/php7/conf.d/50_php.ini
fi

if [ -n "${PHP_POST_MAX_FILESIZE}" ]
then
  sed -i 's/post_max_size = 100M/post_max_size = '"${PHP_POST_MAX_FILESIZE}"',g' /etc/php7/conf.d/50_php.ini
fi

if [ -n "${PHP_ERROR_REPORTING}" ]
then
  sed -i 's/error_reporting = E_ALL/error_reporting = '"${PHP_ERROR_REPORTING}"',g' /etc/php7/conf.d/50_php.ini
fi

if [ -n "${PHP_DISPLAY_ERRORS}" ]
then
  sed -i 's/display_errors = On/display_errors = '"${PHP_DISPLAY_ERRORS}"',g' /etc/php7/conf.d/50_php.ini
fi

# Set xdebug
if [ -n "${PHP_XDEBUG_REMOTE_PORT}" ]
then
  sed -i 's/xdebug.remote_port=9001/xdebug.remote_port='"${PHP_XDEBUG_REMOTE_PORT}"'/g' /etc/php7/conf.d/50_xdebug.ini
fi

if [ -n "${PHP_XDEBUG_IDEKEY}" ]
then
  sed -i 's/xdebug.idekey=xdebug.atom/xdebug.idekey='"${PHP_XDEBUG_IDEKEY}"'/g' /etc/php7/conf.d/50_xdebug.ini
fi

if [ $PHP_XDEBUG_DISABLED = 1 ]
then
  sed -i 's/zend_extension=xdebug.so/#zend_extension=xdebug.so/g' /etc/php7/conf.d/50_xdebug.ini
fi

# Exec PHP-FPM
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm7 "$@"
fi

exec "$@"
