This PHP image :

+ is based on Alpine
+ runs PHP 7 in FPM mode, for coupling with an Apache / NGINX container
+ runs PHP-FPM with UID=1000 and GID=1000 (which is generally corresponding to the user/group of your own user in Ubuntu, so you don't have any permission problems)
+ has the usual PHP extensions for Drupal (GD, ...) and xDebug
+ is for dev purposes, NOT PRODUCTION

Use it in a docker-compose.yml like:

    version: '2'

    services:

      php7:
        container_name: php7
        image: guillaumeduveau/php7
        volumes:
        # your mappings here.
        - ./sites:/sites
        # Uncomment those lines if you want to enable Xdebug.
        #environment:
          #PHP_XDEBUG_DISABLED: 1

Simple script to enable or disable xDebug on the fly:

    #!/bin/bash
    if [ "$EUID" = 0 ]; then
      if [ "$1" = "on" ]; then
        docker exec -i -t php7 sh -c "sed -i 's/#zend_extension=xdebug.so/zend_extension=xdebug.so/g' /etc/php7/conf.d/50_xdebug.ini && kill -USR2 1"
        echo "xDebug ON"
      elif [ "$1" = "off" ]; then
        docker exec -i -t php7 sh -c "sed -i 's/zend_extension=xdebug.so/#zend_extension=xdebug.so/g' /etc/php7/conf.d/50_xdebug.ini && kill -USR2 1"
        echo "xDebug OFF"
      else
        echo "Parameters: on or off"
      fi
    else
      echo "You must sudo."
    fi

Put this in /usr/local/bin/xdebug, chmod +x it and use with sudo, ex: *sudo xdebug on*
