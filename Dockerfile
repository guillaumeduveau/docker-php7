FROM alpine:3.8
LABEL maintainer "guillaume.duveau@gmail.com"

# Ensure www-data user exists
RUN addgroup -g 82 -S www-data && \
    adduser -u 82 -D -S -G www-data www-data

# Upgrade
RUN apk upgrade --no-cache

# Install PHP with extensions
RUN apk add --no-cache \
            php7 \
            php7-bcmath \
            php7-dom \
            php7-ctype \
            php7-curl \
            php7-fileinfo \
            php7-fpm \
            php7-gd \
            php7-iconv \
            php7-intl \
            php7-json \
            php7-memcached \
            php7-mbstring \
            php7-mcrypt \
            php7-mysqli \
            php7-mysqlnd \
            php7-opcache \
            php7-openssl \
            php7-pdo \
            php7-pdo_mysql \
            php7-pdo_pgsql \
            php7-pdo_sqlite \
            php7-phar \
            php7-posix \
            php7-session \
            php7-simplexml \
            php7-soap \
            php7-tokenizer \
            php7-xdebug \
            php7-xml \
            php7-xmlreader \
            php7-xmlwriter \
            php7-zip \
            php7-zlib

# Configure PHP-FPM
RUN sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php7/php-fpm.conf && \
    sed -i 's/listen = 127.0.0.1:9000/listen = [::]:9000/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/user = nobody/user = www-data/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/group = nobody/group = www-data/g' /etc/php7/php-fpm.d/www.conf

# Run PHP-FPM as www-data with different UID=1000 and GID=1000
RUN deluser www-data && \
    addgroup -g 1000 -S www-data && \
    adduser -u 1000 -D -S -s /bin/bash -G www-data www-data && \
    sed -i '/^www-data/s/!/*/' /etc/shadow

# Configure PHP
COPY etc/php7/conf.d/*.ini /etc/php7/conf.d/
RUN rm /etc/php7/conf.d/xdebug.ini

# Fix iconv
# @see https://github.com/docker-library/php/issues/240#issuecomment-305038173
RUN apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

# Docker entrypoint
COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

# Expose port
EXPOSE 9000

CMD ["php-fpm7"]
